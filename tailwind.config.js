/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./**/*.{html,js}",
  ],
  theme: {
    container:{
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '7rem',
        '2xl': '9rem',
      },
    },
    extend: {
      colors:{
        "first-bg-c":"#0D28A6",
        "second-bg-c":"#F1F3FF",
        "third-bg-c":"#CFD4ED",
        "first-bg-c-btn":"#5CB85F",
        "first-c-text":"#ffffff",
        "primary-bg-c":"#0D28A6",
        "fourth-bg-c":"#F1F3FF"
      },
      fontSize:{
        "first-size-h":"36px",
        "second-size-h":"24px",
        "first-size-t":"14px"
      },
      fontFamily:{
        "myFont" :["Montserrat"],
      },
      keyframes:{
        wiggle:{
          '0%': { transform: 'translateX(50%)' },
        },
        await:{
          "0%":{transform:'translateX(100%)'},
        },
        bubble1:{
          "50%":{ transform:'translateX(50%) translateY(-30%)'}
        }
      },
      animation:{
        bubble1:"bubble1 6s linear infinite",
        slider:"wiggle 400ms linear",
        await:"await 400ms linear 10s"
      }
    },
  },
  plugins: [],
}
