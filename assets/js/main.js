 // handle button humberger
 const btn_humberger = document.getElementById("humberger");
 btn_humberger.addEventListener("click", function (e) {
     e.preventDefault();
     this.classList.toggle("humberger-active");
     this.previousElementSibling.classList.toggle("hidden");
     this.previousElementSibling.classList.toggle("nav-sidebar");
     this.parentElement.parentElement.classList.toggle("mt-2");
   

 });
 // swiper
 const swiper = new Swiper(".mySwiper", {
     slidesPerView: 1,
     spaceBetween: 30,
     pagination: {
         el: ".swiper-pagination",
         clickable: true,
     },
     breakpoints: {
         640: {
             slidesPerView: 2
         },
         1024: {
             slidesPerView: 3
         }
     }
 });
 //end swiper
 // handle FAQ
 const containerFaq=document.getElementById("faq");
 containerFaq.addEventListener("click",function(e){
     e.preventDefault();

     if(e.target.className.includes("fa-angle-down")){
     // get element more info remove angle down
         e.target.parentElement.parentElement.previousElementSibling.lastElementChild.classList.remove("truncate");
         e.target.parentElement.nextElementSibling.classList.remove("hidden");

         e.target.parentElement.classList.add("hidden");
     }
     else if (e.target.className.includes("fa-minus")){
         e.target.parentElement.parentElement.previousElementSibling.lastElementChild.classList.add("truncate");
         e.target.parentElement.previousElementSibling.classList.remove("hidden");
         e.target.parentElement.classList.add("hidden");
     }
 })